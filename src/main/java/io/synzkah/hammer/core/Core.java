/*******************************************************************************
 * Copyright (C) 2018
 * Powered by Synzkah - <synzkah@gmail.com>
 * Synzkah.io - HammerMC.fr
 *
 * Core can not be copied and/or distributed without permission of Synzkah
 * Software powered by Synzkah, only HammerMC.fr can use it.
 * Under license: https://synzkah.io/HammerMC/Core.LICENSE
 ******************************************************************************/

package io.synzkah.hammer.core;

import java.lang.reflect.Modifier;
import java.util.List;
import java.util.logging.Level;

import org.bukkit.Location;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.potion.PotionEffect;

import com.google.common.collect.Lists;

import io.synzkah.hammer.core.configuration.Configuration;
import io.synzkah.hammer.core.i18n.Lang;
import io.synzkah.hammer.core.managers.AbstractManager;
import io.synzkah.hammer.core.managers.CommandManager;
import io.synzkah.hammer.core.managers.ConfigurationManager;
import io.synzkah.hammer.core.managers.DatabaseManager;
import io.synzkah.hammer.core.managers.HookManager;
import io.synzkah.hammer.core.managers.LangManager;
import io.synzkah.hammer.core.managers.SotwManager;
import io.synzkah.hammer.core.utils.json.adapters.ItemStackAdapter;
import io.synzkah.hammer.core.utils.json.adapters.LocationAdapter;
import io.synzkah.hammer.core.utils.json.adapters.PotionEffectAdapter;
import lombok.Getter;
import lombok.Setter;
import net.minecraft.util.com.google.gson.Gson;
import net.minecraft.util.com.google.gson.GsonBuilder;

@Getter @Setter
public class Core extends JavaPlugin {
	
	/**
	 * TODO: 
	 * 		* Pretty Printing
	 * 		* Hook with Vault, Essentials & Mango
	 * 		* Verify if all is Successfully launch
	 */
	
	
	/**
	 * Ordre: 
	 * 		D'abord on load la Configuration, normal
	 *		Puis les Lang 
	 *		Ensuite on check l'hook avec Vault & Mango 
	 *		Ensuite les commandes 
	 *		La database, le core est semi-load on check si tout va bien 
	 */

    public static Core i;
    @Getter private Gson gson;
    @Getter private Configuration configuration;
    @Getter private Lang lang;
    
    /**
     * Info
     */
    
    @Getter private String prefix;
    @Getter private boolean debug;
    @Getter private Long startTime;
    
    /**
     * Lang
     */

    /**
     * Managers
     */
    
    private List<AbstractManager> managers = Lists.newArrayList();
    
	private ConfigurationManager configurationManager;
	private LangManager langManager;
	private HookManager hookManager;
	private CommandManager commandManager;
	private DatabaseManager databaseManager;
	
	private SotwManager sotwManager;
    
    /**
     * @Constructor
     */
    
    public Core() {
    	
    	i = this;
    	startTime = System.currentTimeMillis();
    	this.gson = this.getGsonBuilder().create();
    	
    	/**
    	 * Informations 
    	 */
    	
    	this.startTime = System.currentTimeMillis();
    	this.prefix = "";
    	this.debug = true;
    }
    
    @Override
    public void onEnable() {
    	
    	preEnable();
    	
    	/**
    	 * Enabling Manager's
    	 */

        managers.add(this.configurationManager = new ConfigurationManager(this));
        managers.add(this.langManager = new LangManager(this));
        managers.add(this.hookManager = new HookManager(this));
        managers.add(this.commandManager = new CommandManager(this));
        managers.add(this.databaseManager = new DatabaseManager(this));
        
        managers.add(this.sotwManager = new SotwManager(this));
        
        managers.forEach(AbstractManager::onEnable);

        postEnable();
        
    }
    
    
    private GsonBuilder getGsonBuilder() {
    	return new GsonBuilder()
        		.setPrettyPrinting()
        		.disableHtmlEscaping()
        		.serializeNulls()
        		.excludeFieldsWithModifiers(Modifier.TRANSIENT, Modifier.VOLATILE)
        		.setDateFormat("dd/MM/yyyy HH:mm")
        		.registerTypeHierarchyAdapter(ItemStack.class, new ItemStackAdapter())
        		.registerTypeAdapter(PotionEffect.class, new PotionEffectAdapter())
        		.registerTypeAdapter(Location.class, new LocationAdapter());
    }
    
    /**
     * Fired when Plugin starting to be Enabled
     */
    
    private void preEnable() {
    	
    	
    	
    }
    
    /**
     * Fired only in Emergency..
     */
    
    public void shutdown() {
    	
    	this.getLogger().log(Level.SEVERE, "Core is going to be Shutdown!");
    	this.setEnabled(false);
    	this.getPluginLoader().disablePlugin(this);
    	
    }
    
    private void postEnable() {

    }
    
    @Override
    public void onDisable() {
    	managers.forEach(AbstractManager::onDisable);
    }
}
