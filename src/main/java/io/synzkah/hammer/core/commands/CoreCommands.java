package io.synzkah.hammer.core.commands;

import io.synzkah.skCommand.Command;
import io.synzkah.skCommand.CommandArgs;

public class CoreCommands {
	
	@Command(name = "core",
			description = "Core main command", 
			usage = "/core")
	public void core(CommandArgs args) {
	}

}
