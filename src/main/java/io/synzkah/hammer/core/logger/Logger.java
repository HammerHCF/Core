package io.synzkah.hammer.core.logger;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;

import io.synzkah.hammer.core.Core;
import io.synzkah.hammer.core.utils.Utilities;

public class Logger {
	
	public static void log(String message) {
		Bukkit.getConsoleSender().sendMessage(Utilities.color("[Core]" + message));
	}
	
	public static void info(String message) {
		log("&b" + message);
	}

	public static void error(String message) {
		log("&8[&cERROR&8] &c" + message);
	}
	
	public static void debug(String message) {
		if(!Core.i.isDebug()) return;
			log("&e" + message);
	}
	
	public static void line(ChatColor color) {
		log(color + Utilities.LINE);
	}
}
