package io.synzkah.hammer.core.utils;

/**
 * @author Synzkah
 *	25 aug. 2018
 */

public interface Saveable {
	
	/**
	 * Load
	 */
	
	public void load();
	
	/**
	 * Save
	 */
	
	public void save();

}
