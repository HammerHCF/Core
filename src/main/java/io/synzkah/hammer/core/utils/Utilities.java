/*******************************************************************************
 * Copyright (C) 2018
 * Powered by Synzkah - <synzkah@gmail.com>
 * Synzkah.io - HammerMC.fr
 *
 * Core can not be copied and/or distributed without permission of Synzkah
 * Software powered by Synzkah, only HammerMC.fr can use it.
 * Under license: https://synzkah.io/HammerMC/Core.LICENSE
 ******************************************************************************/

package io.synzkah.hammer.core.utils;

import org.bukkit.ChatColor;

public class Utilities {
	
	public static String LINE = ChatColor.STRIKETHROUGH + "-----------------------------------------------------";

    public static String color(String message) {
        return ChatColor.translateAlternateColorCodes('&', message);
    }
}
