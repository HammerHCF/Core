package io.synzkah.hammer.core.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import org.bukkit.Bukkit;

import io.synzkah.hammer.core.Core;
import io.synzkah.hammer.core.utils.json.Persistance;

public class Disk {
	
	private final static String UTF8 = "UTF-8";
	private static HashMap<String, Lock> locks = new HashMap<String, Lock>();
	
	/**
	 * UTF-8 string
	 * @param string to encode
	 * @return byte[] encoded
	 */
	
	public static byte[] utf8(String string) {
		try { 
			return string.getBytes(UTF8); 
		} catch (UnsupportedEncodingException e) { e.printStackTrace();}
		
		return null;
	}

	/**
	 * UTF-8 byte[]
	 * @param bytes to encode
	 * @return string encoded
	 */
	
	public static String utf8(byte[] bytes) {
    	try {
    		return new String(bytes, UTF8);
    	} catch (UnsupportedEncodingException e) { e.printStackTrace(); }
    	
    	return null;
	}
	
	/**
	 * Read File without Try
	 * @param file to read
	 * @return content
	 */
	
	public static String readCatch(File file) {
		try { return read(file); } 
			catch (IOException e) { return null; }
	}
	
	/**
	 * Read File
	 * @param file to read
	 * @return content
	 * @throws 
	 * 		IOException
	 */
	
	public static String read(File file) throws IOException {
		return utf8(readBytes(file));
	}
	
	/**
	 * Read Bytes
	 * @param file to read bytes
	 * @return byte[] read from File
	 * @throws 
	 * 		IOException
	 */
	
	public static byte[] readBytes(File file) throws IOException {
		int offset = 0;
		int length = (int) file.length();
		byte[] output = new byte[length];
		
		InputStream in = new FileInputStream(file);
		
		while (offset < length) 
			offset += in.read(output, offset, (length - offset));
		
		in.close();
		
		return output;
	}
	
	/**
	 * Write string
	 * 
	 * @param file to write
	 * @param content to write
	 * @throws 
	 * 		IOException
	 */
	
	public static void write(File file, String content) throws IOException {
		writeBytes(file, utf8(content));
	}
	
	/**
	 * Write bytes 
	 * 
	 * @param file to write
	 * @param bytes to write
	 * @throws 
	 * 		IOException
	 */
	
	public static void writeBytes(File file, byte[] bytes) throws IOException {
		FileOutputStream out = new FileOutputStream(file);
		out.write(bytes);
		out.close();
	}
	
	/**
	 * Write File without Try
	 * @param file to write
	 * @param content to write
	 * @return success
	 */
	
	public static boolean writeCatch(File file, String content) {
		byte[] bytes = utf8(content);
		String name = file.getName();
		Lock lock;

		if (locks.containsKey(name)) lock = locks.get(name);
        
		else {
			ReadWriteLock rwl = new ReentrantReadWriteLock();
			lock = rwl.writeLock();
			locks.put(name, lock);
        }

        lock.lock();
        
        try {
        	FileOutputStream out = new FileOutputStream(file);
        	out.write(bytes);
        	out.close();
        } catch (IOException e) { e.printStackTrace(); } 
        
        finally {
        	lock.unlock();
        } 

        return true;
	}
	
	/**
	 * Write File Async
	 * @param file to write
	 * @param content to write
	 */
	
	public static void writeAsync(File file, String content) {
		Bukkit.getScheduler().runTaskAsynchronously(Core.i, () -> { 
			writeCatch(file, content);
		});
	}
	
	/**
	 * Create directory
	 * @param directory to create
	 */
	
	public static void createDirectory(File directory) {
		if(!directory.exists())
			directory.mkdirs();
	}
	
	/**
	 * Return directory
	 * @param directory to create and return
	 */
	
	public static File getDirectory(File directory) {
		createDirectory(directory);
		return directory;
	}
	
	/**
	 * Create File 
	 * @param directory of the file
	 * @param name of the file
	 * @return File
	 */
	
	public static File createFile(File directory, String name) {
		if(!directory.exists())
			directory.mkdirs();
		
		return new File(directory, name);
	}
	
	/**
	 * Generate a Persistance File
	 * @param name of the Persistance
	 * @return File
	 */
	
	public static File createPersistance(String name) {
		return new File(Persistance.directory, name.toLowerCase() + ".json");
	}
}
