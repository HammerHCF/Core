package io.synzkah.hammer.core.utils.json;

import java.io.File;

import io.synzkah.hammer.core.Core;
import io.synzkah.hammer.core.utils.Disk;
import io.synzkah.hammer.core.utils.Saveable;
import net.minecraft.util.com.google.gson.Gson;

/**
 * @author Synzkah
 *	26 aug. 2018
 */

public interface Persistance extends Saveable {

	/**
	 * Directory where Persistances are save
	 */
	
	File directory = Disk.getDirectory(new File(Core.i.getDataFolder() + "/persistances/"));
	
	/**
	 * GSON instance
	 */
	
	Gson gson = Core.i.getGson();
	
	/**
	 * Return persistance File
	 */
	
	File getFile();
	
}