/*******************************************************************************
 * Copyright (C) 2018
 * Powered by Synzkah - <synzkah@gmail.com>
 * Synzkah.io - HammerMC.fr
 *
 * Core can not be copied and/or distributed without permission of Synzkah
 * Software powered by Synzkah, only HammerMC.fr can use it.
 * Under license: https://synzkah.io/HammerMC/Core.LICENSE
 ******************************************************************************/

package io.synzkah.hammer.core.configuration;

import java.util.List;

import org.bukkit.Location;

import lombok.Data;
import net.minecraft.util.com.google.common.collect.Lists;

@Data
public class Configuration {
	
	private List<String> sotwWhitelistedPlayer;
    private Location spawn;
    
    public Configuration() {
    	
    	this.sotwWhitelistedPlayer = Lists.newArrayList();
    	
    }

}
