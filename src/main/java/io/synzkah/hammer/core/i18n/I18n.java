package io.synzkah.hammer.core.i18n;

import java.text.MessageFormat;

public class I18n {
	
	public static String tl(Lang lang, String key, String... values) {
		
		if(lang.getWords().containsKey(key)) {
			String text = lang.getWords().get(key);
            MessageFormat messageFormat = new MessageFormat(text);
            return messageFormat.format(values);
		}
		
		return "�4Missing translation for " + key;
		
	}
	
	

}
