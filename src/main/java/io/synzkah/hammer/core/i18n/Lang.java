package io.synzkah.hammer.core.i18n;

import java.util.HashMap;

import lombok.Data;
@Data
public class Lang {
	
	private String name;
	private String code;
	private HashMap<String, String> words;
	
	public Lang() {
		this.words = new HashMap<String, String>();
		
		assertWord("lang", "Default");
	}
	
	public Lang withName(String name) { this.setName(name); return this; }
	public Lang withCode(String code) { this.setCode(code); return this; }
	public Lang assertWord(String key, String value) { this.getWords().put(key, value); return this; }

}