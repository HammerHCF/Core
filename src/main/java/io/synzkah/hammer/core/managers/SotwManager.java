package io.synzkah.hammer.core.managers;

import io.synzkah.hammer.core.Core;
import io.synzkah.hammer.core.data.sotw.SotwData;
import io.synzkah.hammer.core.data.sotw.SotwDataManager;
import lombok.Getter;

@Getter
public class SotwManager extends AbstractManager {
	
	@Getter private static SotwManager instance;
	private SotwData data;
	
	public SotwManager(Core core) {
		
		instance = this;
		this.core = core;
		this.data = SotwDataManager.i.getData();
	}

	@Override
	public void onEnable() {
		
		// TODO: Check if is the SOTW 
		
		if(data.isAlreadyStarted())  {
			
		}
		 
	}
	
	@Override
	public void onDisable() {
		
	}
}
