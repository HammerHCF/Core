package io.synzkah.hammer.core.managers;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.List;

import com.google.common.collect.Lists;

import io.synzkah.hammer.core.Core;
import io.synzkah.hammer.core.commands.CoreCommands;
import io.synzkah.hammer.core.utils.Utilities;
import io.synzkah.skCommand.Command;
import io.synzkah.skCommand.CommandFramework;
import lombok.Getter;
import net.md_5.bungee.api.ChatColor;

/**
 * @author Synzkah
 *	29 aug. 2018
 */

@Getter
public class CommandManager extends AbstractManager {
	
	public static CommandManager i;
	private CommandFramework framework;
	private HashMap<Object, Command> registeredCommands = new HashMap<Object, Command>();
	
	/**
	 * CommandManager
	 * @param core instance
	 */

	public CommandManager(Core core) {
		this.core = core;
		
		i = this;
		this.framework = new CommandFramework(core);
		this.framework.registerHelp();
	}

	@Override
	public void onEnable() {
		
		this.log(ChatColor.GRAY + Utilities.LINE);
		this.log(ChatColor.GREEN + "Enabling..");
		
		this.register(new CoreCommands());
		
		this.log(ChatColor.GREEN + "Enabled!");
		this.log(ChatColor.GRAY + Utilities.LINE);
	}
	
	/**
	 * Register Command(s)
	 * @param object
	 */
	
	public void register(Object object) {
		
		List<Command> commands = Lists.newArrayList();
		
		/**
		 * Attempt to register Commands
		 */
		
		for(Method method: object.getClass().getMethods()) {
			if(method.isAnnotationPresent(Command.class)) {
				Command command = method.getAnnotation(Command.class);
				
				commands.add(command);
				this.registeredCommands.put(object, command);
				this.framework.registerCommands(object);
				
				this.log(ChatColor.GRAY + "> &6/" + command.name() + " &asuccessfully register!");
			}
		}
	}

	@Override
	public void onDisable() {
		
		this.log(ChatColor.RED + "Disabling CommandManager...");
		
	}
	
	
}
