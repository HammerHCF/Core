package io.synzkah.hammer.core.managers;

import java.io.File;
import java.util.Arrays;
import java.util.List;

import io.synzkah.hammer.core.Core;
import io.synzkah.hammer.core.i18n.Lang;
import io.synzkah.hammer.core.logger.Logger;
import io.synzkah.hammer.core.utils.Disk;
import lombok.Getter;
import net.minecraft.util.com.google.common.collect.Lists;
import net.minecraft.util.com.google.common.reflect.TypeToken;
import net.minecraft.util.org.apache.commons.io.FilenameUtils;

@Getter 
public class LangManager extends AbstractManager {
	@Getter private static LangManager instance;
	private File directory = Disk.getDirectory(new File(Core.i.getDataFolder() + "/langs/"));
	private List<Lang> langs = Lists.newArrayList();
	
	public LangManager(Core core) {
		this.core = core;
		instance = this;
	}

	@Override @SuppressWarnings("serial")
	public void onEnable() {
		Long start = System.currentTimeMillis();
		List<File> files = Arrays.asList(directory.listFiles());
		List<File> langFiles = Lists.newArrayList();
		
		Logger.debug("Find in Langs directory " + files.size() + " files: ");
		files.forEach(file -> Logger.debug(FilenameUtils.getBaseName(file.getName())));
		
		/**
		 * Check if File is json
		 */
		
		for(File file: files) {
			if(FilenameUtils.getExtension(file.getName()).equals("json")) {
				langFiles.add(file);
			}
		}
		
		/** 
		 * Folder is empty, or not Lang files can be found..
		 */
		
		if(langFiles.isEmpty()) {
			Logger.error("Can't found any Lang.. Creating default Lang!");
			createDefaultLang();
			return;
		}
		
		/** 
		 * Load
		 */
		
		Logger.debug("Found " + langFiles.size() + " files who can be Lang!");
		
		for(File langFile: langFiles) {
			
			String content = Disk.readCatch(langFile);
			
			if(!(content == null)) {
				
				try {
					
					Lang lang = Core.i.getGson().fromJson(content, new TypeToken<Lang>(){}.getType());
					this.langs.add(lang);
					Logger.debug(lang.getName() + " lang files is now successfully loaded!");
					
				} catch(Exception exception) {
					
					exception.printStackTrace();
					
				}
			}
		}
		
		Logger.debug(langs.size() + " lang loaded in " + (System.currentTimeMillis() - start) + " ms.");
	}
	
	/**
	 * Create default lang
	 */
	
	private void createDefaultLang() {

		// TODO: Pretty Printing 
		
		Logger.error("No default Lang found.. Attempt to create new default Lang. Please shutdown and edit.");
		Lang lang = new Lang().withName("Default").withCode("default").assertWord("How", "Assert word using /I18n assert <key> <word>");
		this.langs.add(lang);
		Logger.info("Lang created. Do /I18n assert <key> <word>");
		
	}
	
	/**
	 * Parse Lang from 
	 * 
	 * @param 
	 * 		code of the Lang
	 * 
	 * @return Lang
	 */
	
	public Lang parse(String code) {
		return langs.stream().filter(lang -> code.equals(lang.getCode())).findAny().orElse(null); 
	}

	@Override
	public void onDisable() {
	
		Long start = System.currentTimeMillis();
		
		/**
		 * Saving all Langs
		 */
		
		Logger.info("Starting to try to write " + langs.size() + " lang to disk.");
		
		try {
			
			for(Lang lang: langs) {
				
				Long startSavingLang = System.currentTimeMillis();
				String fileName = lang.getCode().toLowerCase() + ".json";
				
				/**
				 * Saving Lang
				 */
				
				Logger.debug("Trying to save Lang=" + lang.getName() + " into " + fileName);
				Disk.writeCatch(new File(this.getDirectory(), fileName), Core.i.getGson().toJson(lang));
				Logger.debug("Successfully saved Lang=" + lang.getName() + " into " + fileName + " in " + (System.currentTimeMillis() - startSavingLang) + " ms.");
			}
			
		} catch(Exception exception) {
			
			exception.printStackTrace();
			
		}
		
		Logger.info("Successfully saved " + langs.size() + " lang in " + (System.currentTimeMillis() - start) + " ms.");
	}

}
