package io.synzkah.hammer.core.managers;

import org.bukkit.plugin.RegisteredServiceProvider;

import io.synzkah.hammer.core.Core;
import io.synzkah.hammer.core.logger.Logger;
import lombok.Getter;
import net.milkbowl.vault.economy.Economy;

@Getter 
public class HookManager extends AbstractManager {
	
	private Economy economy;
	
	public HookManager(Core core) {
		this.core = core;
	}

	@Override
	public void onEnable() {
		
		this.log("Enabling..");
		
		/**
		 * Vault ? 
		 */
		
		if (core.getServer().getPluginManager().getPlugin("Vault") == null) {
			Logger.error("&4FATAL: VAULT CAN'T BE HOOKED! SHUTDOWNING CORE");
			core.shutdown();
            return;
        }
		
		RegisteredServiceProvider<Economy> rsp = core.getServer().getServicesManager().getRegistration(Economy.class);
		
        if (rsp == null) 
            return;
        
        this.economy = rsp.getProvider();
	}

	@Override
	public void onDisable() {}

}
