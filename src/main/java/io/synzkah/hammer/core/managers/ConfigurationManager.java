/*******************************************************************************
 * Copyright (C) 2018
 * Powered by Synzkah - <synzkah@gmail.com>
 * Synzkah.io - HammerMC.fr
 *
 * Core can not be copied and/or distributed without permission of Synzkah
 * Software powered by Synzkah, only HammerMC.fr can use it.
 * Under license: https://synzkah.io/HammerMC/Core.LICENSE
 ******************************************************************************/

package io.synzkah.hammer.core.managers;

import java.io.File;

import io.synzkah.hammer.core.Core;
import io.synzkah.hammer.core.configuration.Configuration;
import io.synzkah.hammer.core.logger.Logger;
import io.synzkah.hammer.core.utils.Disk;
import lombok.Getter;
import net.minecraft.util.com.google.gson.reflect.TypeToken;

/**
 * @author Synzkah
 *  20 aug. 2018
 */

public class ConfigurationManager extends AbstractManager {
    @Getter private static ConfigurationManager instance;
	@Getter private File file;
    private Configuration configuration;
    

    /**
     * @Constructor
     */
    
	public ConfigurationManager(Core core) {
		this.core = core;
		instance = this;
		file = Disk.createFile(new File(core.getDataFolder() + "/configurations/"), "configuration.json");
	}

	@Override
	public void onEnable() {
        String content = Disk.readCatch(this.getFile());

        if(content == null) 
        	return;

        try { 
        	this.configuration = core.getGson().fromJson(content, new TypeToken<Configuration>(){}.getType()); 
        } 
        catch(Exception exception) {
            exception.printStackTrace();
        } 
        
        finally { 
        	core.setConfiguration(this.getConfiguration());
        	Logger.info("Configuration loaded!");
        }
	}
	
    public Configuration getConfiguration() {
    	if (this.configuration == null) return this.configuration = new Configuration();
		return this.configuration;
    }

	@Override
	public void onDisable() {
		
		Disk.writeCatch(this.getFile(), core.getGson().toJson(this.getConfiguration()));
		
	}
}
