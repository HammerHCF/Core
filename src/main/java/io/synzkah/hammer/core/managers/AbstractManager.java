package io.synzkah.hammer.core.managers;

import org.bukkit.Bukkit;

import io.synzkah.hammer.core.Core;
import io.synzkah.hammer.core.utils.Utilities;

public abstract class AbstractManager {
	
	protected Core core;
	
	/**
	 * Call when server launching
	 */
	
	public abstract void onEnable();
	
	/**
	 * Called when server are shutdowning
	 */
	
	public abstract void onDisable();
	
	
	/**
	 * Helpers / Utilities
	 */
	
	public void log(String message) { Bukkit.getConsoleSender().sendMessage(Utilities.color("[" + this.getClass().getSimpleName() + "] " + message)); }

}
