package io.synzkah.hammer.core.data.sotw;

import java.io.File;

import io.synzkah.hammer.core.Core;
import io.synzkah.hammer.core.data.IDataManager;
import io.synzkah.hammer.core.utils.Disk;
import lombok.Getter;
import net.minecraft.util.com.google.gson.reflect.TypeToken;

public class SotwDataManager implements IDataManager {

	public static SotwDataManager i;
	
	/**
	 * File
	 */
	
	@Getter private File file = Disk.createFile(directory, "sotw.json");
	
	/**
	 * Data
	 */
	
	private SotwData data;

	@Override
	public void load() {
		
        String content = Disk.readCatch(this.getFile());

        if(content == null) 
        	return;

        /**
         * Try to read Configuration from Disk
         */

        try {

            this.data = Core.i.getGson().fromJson(content, new TypeToken<SotwData>(){}.getType());

        } catch(Exception exception) {

            exception.printStackTrace();
            
        }
	}
	
	public SotwData getData() {
		if(this.data == null)
			return this.data = new SotwData();
		return this.data;
	}
	
	@Override
	public void save() {
		
		Disk.writeCatch(this.getFile(), gson.toJson(this.getData()));
		
	}

}
