package io.synzkah.hammer.core.data;

import java.io.File;

import io.synzkah.hammer.core.Core;
import io.synzkah.hammer.core.utils.Disk;
import io.synzkah.hammer.core.utils.json.Persistance;

public interface IDataManager extends Persistance {
	
	/**
	 * Directory of Data(s)
	 */
	
	File directory = Disk.getDirectory(new File(Core.i.getDataFolder() + "/data/"));

}
