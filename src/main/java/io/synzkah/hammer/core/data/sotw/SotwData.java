package io.synzkah.hammer.core.data.sotw;

import java.util.Date;

import lombok.Data;

@Data
public class SotwData {
	
	/**
	 * Le SOTW dure un temps d�finis par iTaiz.
	 * Il se d�marre grace � une commande simple, /sotw launch <seconds> 
	 * Durant cette p�riode, les joueurs sont en /god
	 * En executant /sotw launch <seconds>, le fichier Data est edit
	 */
	
	/**
	 * SOTW time. (default is null)
	 * 
	 * Server has already a SOTW
	 * Date when SOTW is launch
	 * SOTW is stopped by Admin's?
	 */
	
	private Integer sotwTime;
	
	private boolean alreadyStarted;
	private Date startedDate;
	private boolean stop;

}